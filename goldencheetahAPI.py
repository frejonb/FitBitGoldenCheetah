from configclass import configclass, config_from_file
from funcy import partial, select, filter, last, walk_values
from more_itertools import flatten, split_before
import pandas as pd
from datetime import datetime, timedelta
import os
from PyTlin import k
from copy import deepcopy
import numpy as np

ATTR_GC_FOLDER = 'gc_folder'
GC_CONFIG_FILE = 'gc.conf'

DEFAULT_CONFIG = {
    ATTR_GC_FOLDER: 'GOLDECHEETAH_ATHLETE_FOLDER_PATH_HERE',
}



class GoldenCheetahAPI(configclass):
    def __init__(self, activities=None):
        
        config_file = self.load_config_file(GC_CONFIG_FILE,DEFAULT_CONFIG)

        if config_file:
            self.gc_folder_path = config_file[ATTR_GC_FOLDER]
            self.activities_path = os.path.join(self.gc_folder_path,'activities')
            self.jsonfiles = [os.path.join(self.activities_path, f)
                                    for f in os.listdir(self.activities_path)]
            if activities:
                self.activities = activities
            else:
                self.activities = map(config_from_file, self.jsonfiles)
 
    def __repr__(self):
        return self.activities.__repr__()
 
    def _request_app_setup(self):
        print("The GoldenCheetah athlete's folder needs to be saved into the\
         file located at: {}.".format(GC_CONFIG_FILE))
 
    def _ride_datestr_to_date(self, x):
        return datetime.strptime(x['RIDE']['STARTTIME'].strip(),
                                             '%Y/%m/%d %H:%M:%S %Z').date()
    
    def _filter_activities(self, pred):
        """
        filters GC activities by pred
        """
        return GoldenCheetahAPI(filter(pred, deepcopy(self.activities)))
    
    def _get_tr_name(self, ride):
        return '-'.join(ride['RIDE']['TAGS']['Source Filename']
                                                .split('_')[0].split('-')[4:-1])
    
    def _get_gc_note(self, ride):
        return ride['RIDE']['TAGS']['Notes']
    
    def _has_filename(self, ride):
        return True if ride.get('RIDE').get('TAGS').get('Source Filename')\
                                                                    else False
 
    def get_power_history(self):
        """
        Returns history of FTP , W', CP, Pmax
        """
        path = os.path.join(self.gc_folder_path,'config/power.zones')
        with open(path, 'r') as fdesc:
            lines = fdesc.read().splitlines()

        return k(lines) \
         @ partial(map,lambda x: x.split(':')) @ flatten \
         @ partial(split_before, pred = lambda x: 'DEFAULTS' in x or '/' in x) \
         @ list @ partial(select, lambda x: x[0]!='DEFAULTS') \
         @ (lambda l: [{'dateTime': datetime.strptime(x[0],'%Y/%m/%d'),
            **( k(x[1:]) @ partial(map,lambda y:
             k(y.split('=')) @ partial(map, lambda i: i.strip()) @ tuple @ 'end'
                                                            if '=' in y else y)
                @ dict @ 'end' ) } for x in l ]) @ 'end'
    
    def get_power_at_date(self, date):
        """
        Returns FTP , W', CP, Pmax for date
        """
        return k(self.get_power_history()) \
         @ partial(filter, lambda x:
                        x['dateTime'] <= datetime.strptime(date,'%Y-%m-%d'))\
         @ last @ 'end'
 
    def get_rides_with_note(self, note):
        """
        Returns list of GC activities with particular note/tag
        """
        return self._filter_activities(lambda x:
                                            note in self._get_gc_note(x))
    
    def get_tr_rides(self):
        """
        Returns list of trainerroad GC activities
        """
        return self.get_rides_with_note('trainerroad')
    
    def get_tr_rides_by_name(self, name):
        """
        Returns list of GC activities with particular trainerroad name.
        Example name is in barefeg-2018-07-25-name-37520292.tcx
        """
        return self._filter_activities(lambda x: 
            name == self._get_tr_name(x)
            if self._has_filename(x) else False)
 
    def get_rides_by_date(self, datestr, datestr_end=None):
        """
        Returns list of GC activities between datestr and datestr_end. If datestr_end ommited, returns rides at datestr
        """
        date = datetime.strptime(datestr,'%Y-%m-%d').date()
        date_end = datetime.strptime(datestr_end,'%Y-%m-%d').date() \
                                                        if datestr_end else None

        return self._filter_activities(
            lambda x:(self._ride_datestr_to_date(x) >= date) \
                  and (self._ride_datestr_to_date(x) <= date_end) if date_end else \
                                            date == self._ride_datestr_to_date(x))
 
    def rides_to_names(self):
        """
        Converts list of rides to list of ride names. If no name returns note
        """
        return k(deepcopy(self.activities)) @ partial(map,
            lambda ride: [self._ride_datestr_to_date(ride).strftime("%Y-%m-%d"),
                                                        self._get_tr_name(ride)]
            if self._has_filename(ride)
            else [self._ride_datestr_to_date(ride).strftime("%Y-%m-%d"),
                                            self._get_gc_note(ride)]) @ 'end'
    
    def rides_to_intervals(self):
        """
        Converts list of rides to list of `GCRide` instances.
        """
        return k(deepcopy(self.activities)) @ partial(map, 
        lambda ride:
         k(ride['RIDE']['INTERVALS']) @ partial(map, lambda interval:
         k(ride['RIDE']['SAMPLES'])
          @ partial(filter, lambda data:
          data['SECS'] >= interval['START'] and data['SECS'] < interval['STOP'])
          @    partial(map, lambda data:
            {'SECS': data.get('SECS',np.nan),'WATTS': data.get('WATTS',np.nan),
                                                        'HR': data.get('HR',np.nan)})
          @ list
          @ (lambda x: (interval['NAME'], x)) @ 'end' ) @ dict \
          @ (lambda x: {'RIDE': x,
            'DATE': self._ride_datestr_to_date(ride), 
            'ATHLETE': self.get_power_at_date(
                datetime.strftime(self._ride_datestr_to_date(ride),'%Y-%m-%d'))})
              @ GCRide @ 'end') @ list @ 'end'
 
 
class GCRide:
    
    BINS = [0,55,76,88,95,106,121,1500]
    LABELS = ['Active Recovery', 'Endurance','Tempo','Sweet Spot',
                                        'Threshold','VO2 Max','Anaerobic']
    ZONES = {'Active Recovery': [0,55], 'Endurance': [55,76],
        'Tempo':[76,88],'Sweet Spot': [88,95], 'Threshold': [95,106],
        'VO2 Max': [106,121],'Anaerobic': [121,1500]}
        
    def __init__(self, ride):
        self.ride = walk_values(lambda d: pd.DataFrame(d), ride['RIDE'])
        self.full_ride = pd.concat(self.ride.values(),ignore_index=True)
        self.athlete = ride['ATHLETE']
        self.date = ride['DATE']
        self.date_str = datetime.strftime(ride['DATE'],'%Y-%m-%d')

        self.RESOURCES = {'efficiency_factor': self.ef,
                    'aerobic_decoupling': self.ad,
                    'normalized_power': self.np,
                    'normalized_hr': self.nhr,
                    'intensity_factor': self.If,
                    'TSS': self.tss,
                    'avg_hr': self.ah,
                    'avg_power': self.ap,
                    'avg_power_percentage': self.app,
                    'moving_time': lambda l: self._seconds_to_time_str(self.mt(l)),
                    'elapsed_time': lambda l: self._seconds_to_time_str(self.et(l)),
                    'moving_time_secs': self.mt,
                    'elapsed_time_secs': self.et,
                    }
        
        list(map(lambda methodname,fn: 
                setattr(self, methodname, partial(self._collection_metrics, fn)),
                 self.RESOURCES.keys(),self.RESOURCES.values()))
 
    def __repr__(self):
        return self.ride.__repr__()
    
    def _lap_fn(self, fn):
        return walk_values(fn, self.ride)        
 
    def _collection_metrics(self, metric):
        """
        This implements the methods in GCRide.RESOURCES
        """
        return self._lap_fn(metric)        
    
    def _seconds_to_time_str(self,s):
        return str(timedelta(seconds = int(s))) if not np.isnan(s) else np.nan
    
    def FTP(self):
        return float(self.athlete['FTP'])
        
    def CP(self):
        return float(self.athlete['CP'])
    
    def Pmax(self):
        return float(self.athlete['Pmax'])
        
    def Wp(self):
        return float(self.athlete["W'"])
        
    def np(self, lap):
        """
        Normalized power
        """
        return lap.rolling(30).mean().apply(lambda x: x**4)\
            .mean().apply(lambda x: x**0.25)['WATTS'] if not lap.empty \
                else np.nan
    
    def pz(self,lap):
        """
        Power zones
        """
        return lap.groupby(pd.cut((100*lap/self.FTP())['WATTS'],
                        bins=self.BINS,labels=self.LABELS)).size()\
            .apply(lambda x:100*(x)/(int(lap['WATTS'].count())) if x>0 else 0) if not lap.empty \
                else np.nan
 
    def nhr(self, lap):
        """
        Normalized Heart rate
        """
        return lap.rolling(30).mean().apply(lambda x: x**4)\
            .mean().apply(lambda x: x**0.25)['HR'] if not lap.empty \
                else np.nan
    
    def If(self, lap):
        """
        Intensity factor
        """
        return self.np(lap)/self.FTP()
    
    def tss(self, lap):
        """
        TSS
        """
        return self.mt(lap)*self.np(lap)*self.If(lap)*100.0/(self.FTP()*3600)
    
    def mt(self, lap):
        """
        Moving time seconds
        """
        return int(lap['SECS'].diff().value_counts()[[1.0]][1]) if not lap.empty \
            else np.nan
    
    def et(self, lap):
        """
        Elapsed time seconds
        """
        return int(lap['SECS'].iloc[-1]-lap['SECS'].iloc[0]) if not lap.empty \
            else np.nan
    
    def ap(self, lap):
        """
        Average power
        """
        return lap.mean()[['WATTS']][0]
        
    def app(self, lap):
        """
        Average power percentage of FTP
        """
        return 100*self.ap(lap)/self.FTP()
    
    def ah(self, lap):
        """
        Average heart rate
        """
        return lap[lap.HR & lap.HR != 0].mean()[['HR']][0] if not lap.empty \
            else np.nan
    
    def ef(self, lap):
        """
        Efficiency factor
        """
        return self.ap(lap)/self.ah(lap)
    
    def ad(self, lap):
        """
        Aerobic decoupling
        """
        halfrows = int(lap.shape[0]/2)
        lap1half = lap.iloc[:halfrows]
        lap2half = lap.iloc[halfrows:]
        
        ef1half = self.ap(lap1half)/self.ah(lap1half)
        ef2half = self.ap(lap2half)/self.ah(lap2half)
        return 100.0*(ef1half-ef2half)/ef1half if ef1half != 0 else np.nan
    
    def lap_metrics(self):
        """
        Returns dict of metrics for each lap. Metrics from GCRide.RESOURCES
        """
        return walk_values(lambda lap: 
            walk_values(lambda x: x(lap),self.RESOURCES), self.ride)
    
    def power_zones(self, lap, plot = None):
        """
        Return power zones for lap.
        :param lap: (String) lap name
        :param plot: [Optional] if not None, shows plot
        """
        return self.pz(self.ride[lap]).plot(kind='barh') if plot\
                                                    else self.pz(self.ride[lap])
    
    def full_power_zones(self, plot = None):
        """
        Return power zones for full ride
        :param plot: [Optional] if not None, shows plot
        :param plot: [Optional] if not None, shows plot
        """
        return self.pz(self.full_ride).plot(kind='barh') if plot\
                                                    else self.pz(self.full_ride)
    