import pandas as pd
from funcy import walk_values, partial
from configclass import config_from_file
from PyTlin import k
from more_itertools import flatten

def dictsOfDicts_to_dataframes(dod):
    dict_of_dfs = walk_values(pd.DataFrame, dod)
    return pd.concat(dict_of_dfs.values(), axis=1, keys=dict_of_dfs.keys())

def sleep_time_series(sleepinfo,columns=['minutesAsleep']):
    return pd.DataFrame(list(flatten([[{'dateOfSleep': dateOfSleep,
        **{column: spp[column] for column in columns}} for spp in sp]
                                    for dateOfSleep,sp in sleepinfo.items()])))
                                    
def RHR_time_series(hrinfo):
    return pd.DataFrame([{'dateTime': dateTime, 'RHR': info['value']['restingHeartRate']}
                                    for dateTime,info in hrinfo.items()])


def intraday_hr_dataframe(intrahrinfo):
    return dictsOfDicts_to_dataframes({dateTime:
        info['activities-heart-intraday']['dataset']
                                    for dateTime,info in intrahrinfo.items()})



def intraday_hr_series_for_sleepmin(minAsleepCond, sleepinfo, intrahrinfo):
    sleepts = sleep_time_series(sleepinfo)
    dates = list(sleepts[minAsleepCond(sleepts.minutesAsleep)].dateOfSleep)
    intra_hr = intraday_hr_dataframe(intrahrinfo)

    return intra_hr.loc[:,dates]
