# %%
import os
%matplotlib inline
import pandas as pd
import matplotlib.pyplot as plt
from fitbitAPI import FitBitAPI
from goldencheetahAPI import GoldenCheetahAPI
import pandaswrapper as pdw
from configclass import config_from_file
from funcy import compose, partial, select, walk_values
from PyTlin import k
import datetime
import numpy as np
import glob
from more_itertools import flatten

#Fetch FitBit data
base_date = '2018-06-18' #end_date = '2018-06-21'
fbApi = FitBitAPI()
# # Heartrate and sleep info
# fbApi.update_heart_sleep(base_date,'heartrate.txt', 'sleep.txt')
# 
# # API Request limit on heartrate intraday info
# try:
#     remaining_range = fbApi.update_heart_intraday(base_date,'heartrate-intraday.txt')
# except :
#     pass
# 
# remaining_range
# 




# load from files
heartrateinfo = config_from_file('heartrate.txt')
heartrateintradayinfo = config_from_file('heartrate-intraday.txt')
sleepinfo = config_from_file('sleep.txt')


heartrateinfo['2018-06-20']


sleepts = pdw.sleep_time_series(sleepinfo,columns=['minutesAsleep','timeInBed'])
sleepts['diff'] = sleepts['timeInBed']-sleepts['minutesAsleep']
sleepts.set_index('dateOfSleep')['diff'].plot()
rhr = pdw.RHR_time_series(heartrateinfo)

rhr.set_index('dateTime').plot()

sleepts

#transpose, set dates as columns and sum values of same column name (nore than 1 sleep time per day)
lel = sleepts.transpose()
lel.columns =  lel.head(1).iloc[0,:]
lel = lel.reindex(lel.index.drop('dateOfSleep'))
lel.columns.name = None
lel = lel.groupby(lel.columns, axis = 1).sum()
lel
lel.axes
# I want to add the above to the table below
sts2 = pdw.intraday_hr_series_for_sleepmin(lambda x: x<= 240, sleepinfo, heartrateintradayinfo)

sts2.axes
pd.concat([sts2],keys=['foo'], names=['firstlevel'])

sts2

pd.concat([sts2,sleepts])


pd.concat([sts2,sleepts.reset_index().T], keys=[1,2])

pepo = pdw.intraday_hr_dataframe(heartrateintradayinfo)
pepo['2018-06-20'].set_index('time').plot()

pipo = pepo['2018-06-17']
pipo[(pipo.time > '03:15:00') & (pipo.time < '07:00:00')].set_index('time').plot()
pipo[(pipo.time > '03:45:00') & (pipo.time < '03:55:00')].set_index('time').plot()



from goldencheetahAPI import GoldenCheetahAPI, GCRide
# %matplotlib inline
# import matplotlib.pyplot as plt
from plotly import offline
offline.init_notebook_mode()
import pandas as pd
from funcy import walk_values, walk_keys, select
from more_itertools import flatten
import numpy as np
from configclass import config_from_file
from datetime import datetime, timedelta
#Save trainerroad metrics to file
def save_tr_metrics():
    gcApi = GoldenCheetahAPI()
    tr_gcrides = gcApi.get_tr_rides().rides_to_intervals()
    config_from_file('tr_metrics.json',
        config=list(map(lambda x: 
            {'date_str': x.date_str, 'ftp': x.FTP(),'metrics': x.lap_metrics()},
             tr_gcrides)), update=False)
             
def update_tr_metrics():
    tr_metrics = sorted(config_from_file('tr_metrics.json'), key=lambda x: x['date_str'])
    last_date_p1d = datetime.strftime(
                datetime.strptime(tr_metrics[-1]['date_str'],'%Y-%m-%d').date()
                                                +timedelta(days=1),'%Y-%m-%d')
    new_rides = GoldenCheetahAPI().get_rides_by_date(last_date_p1d,
        datetime.strftime(datetime.today(),'%Y-%m-%d')).get_tr_rides().rides_to_intervals()
    
    old_data = config_from_file('tr_metrics.json')
    old_data.extend(list(map(lambda x: 
        {'date_str': x.date_str, 'ftp': x.FTP(),'metrics': x.lap_metrics()},
         new_rides)))
    config_from_file('tr_metrics.json',config=old_data, update=False)


# save_tr_metrics()
update_tr_metrics()

#list of tr ridess, i have date, ride data and ahtlete data 
#now i can get metrics for each lap of each ride 
#for each ride pick the laps that are say above z2 and compute efficiency factor 
#give {date: {1: 0.5,....},}
tr_metrics = config_from_file('tr_metrics.json')

def filter_tr_intervals(tr_ride_dict, zone, duration_min = None, duration_max = None):
    zl, zh = GCRide.ZONES[zone] if type(zone) == str else \
                                (zone if type(zone) == list else [zone,None])
    return [tr_ride_dict['date_str'],{int(k): 
            {'efficiency_factor': v.get('efficiency_factor'),
            'aerobic_decoupling': v.get('aerobic_decoupling'),
            'avg_power_percentage': v.get('avg_power_percentage'),
            'moving_time_secs': v.get('moving_time_secs')}
            for k,v in tr_ride_dict['metrics'].items() 
            if k.strip().isdecimal() 
             and (v.get('avg_power_percentage') >= zl if zl else True)
             and (v.get('avg_power_percentage') <= zh if zh else True)
             and (v.get('moving_time_secs') >= duration_min if duration_min else True)
             and (v.get('moving_time_secs') <= duration_max if duration_max else True)
             and not np.isnan(v.get('efficiency_factor'))}]


pupu = list(filter(lambda y: bool(y[1]), map(lambda x: filter_tr_intervals(x,0),tr_metrics)))
pupuu = sorted(pupu, key=lambda x: x[0])
#compare efforts from different days 
def eff_date(data, date):
    return pd.DataFrame.from_dict(select(lambda x: x[0] == date, data)[0][1],
     orient='index')[['efficiency_factor']]\
        .rename(index=str,columns={'efficiency_factor':'ef_'+date})

def compare_eff(data, date_list):
    eff_list = [eff_date(data, date) for date in date_list]
    df = eff_list[0].join(eff_list[1:])
    return offline.iplot([{'x':df.index, 'y':df[col], 'name': col}
        for col in df.columns])
    
def compare_progression(data, date_list):
    ef_prog = list(flatten([[(date+':'+str(k),v['efficiency_factor']) 
            for k, v in select(lambda x: x[0] == date, data)[0][1].items()] 
                                        for date in date_list]))
    df = pd.DataFrame(ef_prog,columns=['date','efficiency_factor'])\
                                                            .set_index('date')
    return offline.iplot([{'x':df.index, 'y':df[col], 'name': col}
        for col in df.columns])


compare_eff(pupuu, ['2017-01-12','2018-01-27','2018-02-26','2018-04-14', '2018-08-02'])

compare_eff(pupuu, ['2017-01-18', '2018-08-08'])

eff_date(pupuu,'2017-01-18').join(eff_date(pupuu,'2018-08-08'))
compare_eff(pupuu, ['2017-01-14', '2018-08-04'])
compare_eff(pupuu, ['2018-08-02', '2018-08-04'])
compare_progression(pupuu, ['2017-01-12', '2018-08-08'])
eff_date(pupuu,'2018-07-26')
compare_progression(pupuu, ['2018-07-31', '2018-08-02', '2018-08-04'])
h_ef = list(flatten([[(i[0]+':'+str(k),v['efficiency_factor']) for k, v in i[1].items()] for i in pupuu]))
h_ef_df = pd.DataFrame(h_ef,columns=['date','efficiency_factor'])





h_ef_df.plot(rot=90)
h_ef_df.iloc[2700:].plot(kind='bar',rot=90)
pd.DataFrame.from_dict(
    walk_keys(lambda x: int(x),select(lambda x: x['date_str'] == '2018-08-02',tr_metrics)[0]['metrics']),
     orient='index')
pd.DataFrame.from_dict(select(lambda x: x[0] == '2018-08-02', pupuu)[0][1], orient='index')[['efficiency_factor']]
pd.DataFrame.from_dict(select(lambda x: x[0] == '2017-01-12', pupuu)[0][1], orient='index')[['efficiency_factor']]
pd.DataFrame.from_dict(select(lambda x: x[0] == '2018-04-14', pupuu)[0][1], orient='index')[['efficiency_factor']].plot()
pd.DataFrame.from_dict(select(lambda x: x[0] == '2018-02-26', pupuu)[0][1], orient='index')[['efficiency_factor']].plot()
pd.DataFrame.from_dict(select(lambda x: x[0] == '2018-01-27', pupuu)[0][1], orient='index')[['efficiency_factor']].plot()




pd.DataFrame(walk_keys(int, pepo[0].efficiency_factor()), index=['ef'])




papa = gcApi.get_tr_rides_by_name('8-minute-ftp-test').activities
pepe = gcApi.get_tr_rides_by_name('20-minute-test').activities
[x['RIDE']['TAGS']['Source Filename'] for x in list(pepe)]
[x['RIDE']['TAGS']['Source Filename'] for x in list(papa)]



pepo = GoldenCheetahAPI().get_rides_by_date('2018-08-05')
pipo = pepo.rides_to_intervals()[0]
pipo.avg_power()
pipo.normalized_power()
pipo.FTP()
pipo.efficiency_factor()
pipo.full_power_zones()
pipo.full_ride[['HR','WATTS']].plot()
pipo.power_zones('4 ')

pd.DataFrame.from_dict(pipo.lap_metrics(), orient='columns')





